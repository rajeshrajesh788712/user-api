class Api::V1::UsersController < ApplicationController
    skip_before_action :verify_authenticity_token 
    def index
        if params[:search]
            @users = User.where('username LIKE :search', search: "%#{params[:search]}%")
           
        else
            @users = User.all
        end 
            render json: @users
    end

    def show
        @user = User.find(params[:id])
        render json: @user
    end

    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user
        else
            render error: { error: 'Unable to create User.' }, status: 400
        end
    end

    def update
        @user = User.find(params[:id])
        if @user
            @user.update(user_params)
            render json: { msg: 'User successfully updated.' }, status: 200
        else
            render json: { error: 'Unable to update User' }, status: 400
        end
    end

    def destroy
        @user = User.find(params[:id])
        if @user
            @user.destroy
            render json: { msg: 'User successfully deleted.' }, status: 200
        else
            render json: { error: 'Unable to delete User' }, status: 400
        end
    end

    private

    def user_params
        params.require(:user).permit(:username, :password)
    end
end
