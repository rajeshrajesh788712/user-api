class Api::V1::InterestsController < ApplicationController
    before_action :find_interest, only: [:show, :update, :destroy]

    def index
        @interests = Interest.all
        render json: @interests
    end

    def show
        render json: @interest
    end

    def create
        @interest = Interest.new(interest_params)
        if @interest.save
            render json: @interest
        else
            render error: { error: 'Unable to create User.' }, status: 400
        end
    end

    def update
        if @interest
            @interest.update(interest_params)
            render json: { msg: 'User successfully updated.' }, status: 200
        else
            render json: { error: 'Unable to update User' }, status: 400
        end
    end

    def destroy
        if @interest
            @interest.destroy
            render json: { msg: 'User successfully deleted.' }, status: 200
        else
            render json: { error: 'Unable to delete User' }, status: 400
        end
    end

    private

    def interest_params
        params.require(:interest).permit(:interest, :types, :user_id)
    end

    def find_interest
        @interest = Interest.find(params[:id])
    end
    
end
