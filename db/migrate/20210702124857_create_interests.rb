class CreateInterests < ActiveRecord::Migration[6.1]
  def change
    create_table :interests do |t|
      t.references :user, null: false, foreign_key: true
      t.string :interest
      t.string :types

      t.timestamps
    end
  end
end
